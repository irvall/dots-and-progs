# Setup fzf
# ---------
if [[ ! "$PATH" == */Users/johan/.fzf/bin* ]]; then
  export PATH="$PATH:/Users/johan/.fzf/bin"
fi

# Auto-completion
# ---------------
[[ $- == *i* ]] && source "/Users/johan/.fzf/shell/completion.zsh" 2> /dev/null

# Key bindings
# ------------
source "/Users/johan/.fzf/shell/key-bindings.zsh"

