" Specify a directory for plugins
" - For Neovim: ~/.local/share/nvim/plugged
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.vim/plugged')

" Make sure you use single quotes

" Shorthand notation; fetches https://github.com/junegunn/vim-easy-align
Plug 'junegunn/vim-easy-align'

" Vim processing
Plug 'sophacles/vim-processing'

" Any valid git URL is allowed
Plug 'https://github.com/junegunn/vim-github-dashboard.git'

" On-demand loading
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'tpope/vim-fireplace', { 'for': 'clojure' }

" FSharp
Plug 'fsharp/vim-fsharp', {
      \ 'for': 'fsharp',
      \ 'do':  'make fsautocomplete',
      \}

" Using a tagged release; wildcard allowed (requires git 1.9.2 or above)
Plug 'fatih/vim-go', { 'tag': '*' }

" Plugin options
Plug 'nsf/gocode', { 'tag': 'v.20150303', 'rtp': 'vim' }

" Plugin outside ~/.vim/plugged with post-update hook
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }

" Unmanaged plugin (manually installed and updated)
Plug '~/my-prototype-plugin'

" ClangFormat
Plug 'rhysd/vim-clang-format'

" Dart support
Plug 'dart-lang/dart-vim-plugin'

" Themes ---
" Nord
Plug 'arcticicestudio/nord-vim'
" Gruvbox
Plug 'morhetz/gruvbox'
" Dracula
Plug 'dracula/vim', { 'as': 'dracula' }
" Horizon
Plug 'ntk148v/vim-horizon', {'as': 'horizon'}
" Blueish
Plug 'smallwat3r/vim-simplicity'

" Panda
Plug 'markvincze/panda-vim'

" Language support ---
" Kotlin
Plug 'udalov/kotlin-vim'

" Scala
Plug 'derekwyatt/vim-scala'

" Nerdtree
Plug 'scrooloose/nerdtree'

" Initialize plugin system
call plug#end()
set tabstop=4
set softtabstop=4
set shiftwidth=4
set noexpandtab
syntax on

filetype plugin indent on
" show existing tab with 4 spaces width
set tabstop=4
" when indenting with '>', use 4 spaces width
set shiftwidth=4
" On pressing tab, insert 4 spaces
set expandtab

map <C-K> :pyf /usr/local/share/clang/clang-format.py<cr>
imap <C-K> <c-o>:pyf /usr/local/share/clang/clang-format.py<cr><Paste>
map m :call cursor(0, len(getline('.'))/2)<cr>
map æ "+y

let g:clang_format_path = '/Users/johan/.config/.clang-format'
:set nohlsearch

:colorscheme simplicity-blue 

:autocmd BufNewFile *.cc 0r ~/.config/nvim/templates/skeleton.cc
:autocmd BufNewFile *.zig 0r ~/.config/nvim/templates/skeleton.zig
:autocmd BufNewFile *.html 0r ~/.config/nvim/templates/skeleton.html
:set relativenumber
set runtimepath^=~/.vim runtimepath+=~/.vim/after
let &packpath = &runtimepath
source ~/.vimrc
