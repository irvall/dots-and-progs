# Enable VI Binding
fish_vi_key_bindings

# Aliases
alias vim=nvim
alias please=sudo
alias python=python3
alias pip=pip3
alias llvm_clang="/usr/local/opt/llvm/bin/clang"

# Abbr
abbr cf "vim ~/.config/fish/config.fish"
abbr fconf "fish_config"
abbr cv "vim ~/.config/nvim/init.vim"
abbr pyserve "python -m http.server"
abbr phpserve "php -S localhost:8000"
abbr fsi "fsharpi --consolecolors"
abbr gp "git push"
abbr genpass "date +%s | shasum | base64 | head -c 48 ; echo"
abbr ipy "ipython"
abbr pipu "pip install --upgrade pip"
abbr bi "brew install"
abbr bci "brew cask install"
abbr python python3
abbr gclone "git clone"
abbr cb "cargo build"
abbr cr "cargo run"
abbr d "dart"

## Path for machine learning
export ML_PATH=$HOME/ml

## LLVM
export LLVM_DIR=/usr/local/opt/llvm/lib/cmake/llvm

# Fixes
export LC_ALL=en_US.UTF-8

# Rust
export RUST_LANG=$HOME/.cargo/bin

# Functions
function viewmd
    pandoc -t plain $argv[1] | less
end

## Count files in folder 
function fcount
    eval ls $argv[1] | wc -l | xargs
end

# Generate pretty stronk pwd
function gpwd
    date | md5
end

# Find application running on port
function rport
    lsof -nP -i4TCP:$argv[1] | grep LISTEN
end

# The next line updates PATH for the Google Cloud SDK.
if [ -f '/Users/johan/Desktop/google-cloud-sdk/path.fish.inc' ]; . '/Users/johan/Desktop/google-cloud-sdk/path.fish.inc'; end

####### Path
set -gx PATH /Users/johan/bin /Users/johan/Library/Python/2.7/bin /Users/Johan/go/bin $HOME/.pub-cache/bin /Users/johan/bin/fsharp/FsLexYacc.9.0.2/build/fslex/net46 $PATH $RUST_LANG
###############################3

