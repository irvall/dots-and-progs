export LC_ALL=en_US.UTF-8

# Enable VI Binding
fish_vi_key_bindings

# Aliases
alias "vim"="nvim"

abbr cf "vim ~/.config/fish/config.fish"
abbr fconf "fish_config"
abbr cv "vim ~/.config/nvim/init.vim"
abbr pyserve "python -m SimpleHTTPServer"
abbr phpserve "php -S localhost:8000"
abbr fsi "fsharpi --consolecolors"
abbr gp "git push -u origin master"
abbr genpass "date +%s | shasum | base64 | head -c 48 ; echo"
abbr ipy "ipython"
abbr pipu "pip install --upgrade pip"

# Path
set -gx PATH /Users/johan/bin /Users/johan/Library/Python/2.7/bin /Users/Johan/go/bin $HOME/.pub-cache/bin /Users/johan/bin/fsharp/FsLexYacc.9.0.2/build/fslex/net46 $PATH

